//
//  Superheroes.swift
//  SuperheroesandLaureates
//
//  Created by student on 4/13/19.
//  Copyright © 2019 student. All rights reserved.
//

import UIKit

class SuperheroesTableViewController: UITableViewController {
       let superHeroesURL: String = "https://www.dropbox.com/s/wpz5yu54yko6e9j/squad.json?dl=1"

    override func viewDidLoad() {
        super.viewDidLoad()
        Superheroes.shared.fetchheroesjsonfile()
        NotificationCenter.default.addObserver(self, selector:#selector(superHeroesDelivered),name: NSNotification.Name("Superheroesdelivered"), object:nil)
    }
    
    @objc func superHeroesDelivered(){
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Superheroes.shared.squad.count
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tableView.reloadData()
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "superhero", for: indexPath)
        let mySuperhero = Superheroes.shared.squad[indexPath.row]
        cell.textLabel?.text = "\(mySuperhero.name) (aka: \(mySuperhero.secretIdentity))"
        var powersData = ""
        for i in 0..<mySuperhero.powers.count{
            if i == mySuperhero.powers.count - 1  {
                powersData += mySuperhero.powers[i]
            }else{
                powersData += "\(mySuperhero.powers[i]), "
            }
        }
        cell.detailTextLabel?.text = powersData
        return cell
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "SUPERHEROS"
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 70
    }
    
    override func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        return ""
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 150
    }
    
    
}
