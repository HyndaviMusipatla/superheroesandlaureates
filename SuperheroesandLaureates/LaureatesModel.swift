//
//  LaureatesModel.swift
//  SuperheroesandLaureates
//
//  Created by student on 4/13/19.
//  Copyright © 2019 student. All rights reserved.
//

import Foundation
struct Laureate {
    var firstname: String?
    var surname: String?
    var born: String?
    var died: String?
}

class Laureates {
    
    var laureatesUrl = "https://www.dropbox.com/s/7dhdrygnd4khgj2/laureates.json?dl=1"
    
    static var shared = Laureates()
    
    var laureatesData: [Laureate]
    
    init(){
        laureatesData = []
    }
    
    func fetchLaureates(){
        let urlSession = URLSession.shared
        let url = URL(string: laureatesUrl)
        urlSession.dataTask(with: url!, completionHandler: displayLaureates).resume()
    }
    
    func displayLaureates(data:Data?, urlResponse:URLResponse?, error:Error?)->Void {
        var laureatesSerializedData:[[String:Any]]!
        var firstnameOfLaureate: String?
        var surnameOfLaureate: String?
        var born: String?
        var died: String?
        do {
            try laureatesSerializedData = JSONSerialization.jsonObject(with: data!, options: .allowFragments)  as?  [[String:Any]]
            for i in 0..<laureatesSerializedData!.count {
                firstnameOfLaureate = ((laureatesSerializedData![i]["firstname"] as? String))
                surnameOfLaureate = ((laureatesSerializedData![i]["surname"] as? String))
                born = (laureatesSerializedData![i]["born"] as? String)
                died = (laureatesSerializedData![i]["died"] as? String)
                self.laureatesData.append(Laureate(firstname: firstnameOfLaureate, surname: surnameOfLaureate, born: born, died: died))
            }
            NotificationCenter.default.post(name: NSNotification.Name("Laureatesdelivered"), object: nil)
        } catch {
            print(error)
        }
    }
    
}
