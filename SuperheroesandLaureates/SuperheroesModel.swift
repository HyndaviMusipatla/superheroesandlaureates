//
//  SuperheroesModel.swift
//  SuperheroesandLaureates
//
//  Created by student on 4/13/19.
//  Copyright © 2019 student. All rights reserved.
//

import Foundation
struct Superhero: Codable {
    var squadName: String
    var homeTown: String
    var formed: Int
    var secretBase: String
    var active: Bool
    var members: [Members]
}

struct Members: Codable {
    var name: String
    var age: Int
    var secretIdentity: String
    var powers: [String]
}

class Superheroes {
    
    static var shared = Superheroes()
    
    var squad: [Members]
    
    init(){
        squad = []
    }
    
    let superHerosURL: String = "https://www.dropbox.com/s/wpz5yu54yko6e9j/squad.json?dl=1"
    
    func fetchheroesjsonfile(){
        let urlSession = URLSession.shared
        let url = URL(string: superHerosURL)
        urlSession.dataTask(with: url!, completionHandler: displayheroesjsondata).resume()
    }
    
    func displayheroesjsondata(data:Data?, urlResponse:URLResponse?, error:Error?)->Void {
        do {
            let decoder = JSONDecoder()
            let superheroesDecodedData = try decoder.decode(Superhero.self, from: data!)
            self.squad = superheroesDecodedData.members
            NotificationCenter.default.post(name: NSNotification.Name("Superheroesdelivered"), object: nil)
        }catch {
            print(error)
        }
    }
}
