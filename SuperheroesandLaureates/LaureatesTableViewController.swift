//
//  LaureatesTableViewController.swift
//  SuperheroesandLaureates
//
//  Created by student on 4/13/19.
//  Copyright © 2019 student. All rights reserved.
//

import UIKit

class LaureatesTableViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        Laureates.shared.fetchLaureates()
        NotificationCenter.default.addObserver(self, selector:#selector(laureatesDataDelivered(notification: )),name: NSNotification.Name("Laureatesdelivered"), object:nil)
    }
    
    @objc func laureatesDataDelivered(notification: Notification){
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Laureates.shared.laureatesData.count
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "laureate", for: indexPath)
        let laureateDataForCell = Laureates.shared.laureatesData[indexPath.row]
        let name = cell.viewWithTag(300) as! UILabel
        let years = cell.viewWithTag(400) as! UILabel
        name.text = "\(laureateDataForCell.firstname!) \(laureateDataForCell.surname!)"
        years.text = "\(laureateDataForCell.born!) - \(laureateDataForCell.died!)"
        return cell
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "LAUREATES"
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 70
    }
    
}
